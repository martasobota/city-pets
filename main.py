from flask import Flask
from flask import jsonify

from research import find_pets, file_paths

app = Flask(__name__)


@app.route('/api/v1/<city>', methods=['GET'])
def pets(city):
    return jsonify(find_pets(city))


if __name__ == '__main__':
    app.debug = True
    app.run()
