import csv
import os

from consts import LOCAL_PATH_MASTERS, LOCAL_PATH_PETS

def find_pets(city):
    path_masters, path_pets = file_paths()
    with open(path_masters, newline='') as csvfile: #find masters (pet owners) living in specific city
        masters = csv.reader(csvfile, delimiter=';')
        city_masters = []
        for row in masters:
            if city in row:
                city_masters.append(row[0])

    with open(path_pets, newline='') as csvpets: #find pets owned by city_masters
        pets_csv = csv.reader(csvpets, delimiter=';')
        pets = {'pets': []}
        for master in city_masters:
            for row in pets_csv:
                if row[1] == master:
                    pets['pets'].append(row[0])
    return pets

def file_paths():
    if os.getenv('GAE_APPLICATION', None):
        pass
    else:
        return LOCAL_PATH_MASTERS, LOCAL_PATH_PETS
